variable "name" {
  type        = string
  description = "Project name"
}

variable "environment" {
  type        = string
  description = "Environment name"
  default     = "develop"
}

variable "location" {
  type        = string
  description = "Deployment region"
  default     = "eastus"
}

variable "resource_group_name" {
  type        = string
  description = "Resource group name"
}

variable "account_tier" {
  type        = string
  description = "Storage tier"
  default     = "Standard"
}

variable "account_replication_type" {
  type        = string
  description = "Storage tier"
  default     = "LRS"
}

variable "account_kind" {
  type        = string
  description = "Storage tier"
  default     = "StorageV2"
}

variable "container_name" {
  type        = string
  description = "Storage container name"
}

variable "container_access_type" {
  type        = string
  description = "Storage container access type"
  default     = "container"
}

variable "tags" {
  type        = map
  description = "Additional default tags"
  default     = {}
}
