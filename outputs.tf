output "storage_account_id" {
  value       = azurerm_storage_account.this.id
  description = "Storage Account identifier"
}

output "storage_container_id" {
  value       = azurerm_storage_container.this.id
  description = "Storage Container identifier"
}

output "primary_blob_host" {
  value       = azurerm_storage_account.this.primary_blob_host
  description = "Blob host name"
}
