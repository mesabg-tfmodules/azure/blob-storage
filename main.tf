resource "azurerm_storage_account" "this" {
  resource_group_name       = var.resource_group_name
  location                  = var.location

  name                      = var.name

  account_tier              = var.account_tier
  account_replication_type  = var.account_replication_type
  account_kind              = var.account_kind

  tags                      = local.tags
}

resource "azurerm_storage_container" "this" {
  name                  = var.container_name
  storage_account_name  = azurerm_storage_account.this.name
  container_access_type = var.container_access_type
}
